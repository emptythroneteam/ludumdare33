﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Fence : MonoBehaviour
{
    public List<Sprite> FencesFG;
    public List<Sprite> FencesBG;
    public List<Sprite> FencesBroken;

    private SpriteRenderer FG;
    private SpriteRenderer BG;

    private bool _broken;
    public bool Broken
    {
        get
        {
            return _broken;
        }
        set
        {
            _broken = value;
            BG.sprite = _broken ? FencesBroken[_level] : FencesBG[_level];
        }
    }

    private int _level;
    public int Level
    {
        get
        {
            return _level;
        }
        set
        {
            if(value < FenceCount)
            {
                _level = value;
                FG.sprite = FencesFG[_level];
                BG.sprite = Broken ? FencesBroken[_level] : FencesBG[_level];
            }
        }
    }

    public int Value
    {
        get
        {
            if(_level == 3)
            {
                return 6;
            }
            return _level + 2;
        }
    }

    public int Cost
    {
        get
        {
            if (_level == 0)
            {
                return 5;
            }

            return 10 + _level * 5;
        }
    }

    public int RepairCount { get; set; }

    private int FenceCount
    {
        get
        {
            return Math.Min(FencesBG.Count, Math.Min(FencesFG.Count, FencesBG.Count));
        }
    }

    public void Repair()
    {
        if(Broken)
        {
            RepairCount++;
            Broken = false;
        }
    }

    public void Start()
    {
        RepairCount = 0;
        FG = this.transform.Find("FG Fence").GetComponent<SpriteRenderer>();
        BG = this.transform.Find("BG Fence").GetComponent<SpriteRenderer>();
        FG.sprite = FencesFG[0];
        BG.sprite = FencesBG[0];
        Broken = false;
    }
}
