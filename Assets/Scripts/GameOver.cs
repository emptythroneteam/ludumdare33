﻿using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public static GameOverState State;

    private CanvasGroup _canvas;
    private Image _image;

    public Sprite GoodEnd, BadEnd, Dead;

    public void Start()
    {
        State = GameOverState.Hidden;

        _image = this.GetComponent<Image>();
        _canvas = this.GetComponent<CanvasGroup>();
    }

    public void Update()
    {
        if (State != GameOverState.Hidden)
        {
            switch(State)
            {
                case GameOverState.GameOver:
                    _image.sprite = Dead;
                    break;
                case GameOverState.BadEnd:
                    _image.sprite = BadEnd;
                    break;
                case GameOverState.GoodEnd:
                    _image.sprite = GoodEnd;
                    break;
            }

            _canvas.alpha = _canvas.alpha + Time.deltaTime / 2f;
        }
    }
}
