﻿using UnityEngine;

public class Mother : CharacterBase
{
    public Sheep Target;

    public Sprite Portrait;

    private Animator _animator;

    private AudioSource _audio;

    private float _completeTime;

    private MotherState _state;
    public MotherState State
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
            _animator.SetInteger("State", (int)value);
        }
    }

    public override void Start()
    {
        base.Start();
        _animator = this.GetComponent<Animator>();
        _audio = this.GetComponent<AudioSource>();
    }

    public override void Update()
    {
        base.Update();

        if(State == MotherState.Shear && _completeTime < Time.time)
        {
            Target.State = SheepState.Sheared;
            Target = null;
            State = MotherState.Idle;
        }

        if(State != MotherState.Shear)
        {
            _audio.Stop();
        }
    }

    public override void AtDestination()
    {
        if(Target != null)
        {
            _audio.Play();
            Target.State = SheepState.Shear;
            State = MotherState.Shear;
            _completeTime = Time.time + 2f;
        }
        else
        {
            State = MotherState.Idle;
        }
    }

    public void Shear(Sheep sheep)
    {
        Target = sheep;
        this.GoTo(sheep.transform.position + Vector3.down * 0.25f);
    }

    public override void GoTo(Vector3 target)
    {
        base.GoTo(target);
        State = MotherState.Walk;
    }
}
