﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;

public class DialogProvider : MonoBehaviour, IPointerClickHandler
{
    public TextAsset DialogFile;

    public string Name;

    public Sprite Happy1;

    public Sprite Happy2;

    public Sprite Sad1;

    public Sprite Sad2;

    public Sprite CurrentPortrait
    {
        get
        {
            return Happy1;
        }
    }

    private DialogBox _dialogBox;

    private GameManager _gameManager;

    private List<DialogEntry> _dialogs;

    private List<DialogEntry> CurrentDialogs
    {
        get { return _dialogs.Where(d => d.Days.Contains(_gameManager.Day) && (string.IsNullOrEmpty(d.Condition) || _gameManager.GetCondition(d.Condition))).ToList(); }
    }

    public void Awake()
    {
        _dialogBox = GameObject.Find("DialogBox").GetComponent<DialogBox>();
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();        
        _dialogs = DialogFile.text.Split('\n').Where(s => !string.IsNullOrEmpty(s)).Select<string, DialogEntry>(DialogEntry.ParseDialog).ToList();
    }

    public virtual void OnPointerClick(PointerEventData eventData)
    {
        _dialogBox.ShowDialog(Name, CurrentDialogs[Random.Range(0, CurrentDialogs.Count())].Contents, Happy1);
    }

    private class DialogEntry
    {
        public static DialogEntry ParseDialog(string raw)
        {
            var split = raw.Split('#');
            var entry = new DialogEntry();

            entry.Days = split[0].Split(',').Where(s => !string.IsNullOrEmpty(s)).Select<string, int>(int.Parse).ToList();
            entry.Contents = split[1];

            if(split.Length > 2)
            {
                entry.Condition = split[2];
            }

            return entry;
        }

        public List<int> Days { get; set; }

        public string Contents { get; set; }

        public string Condition { get; set; }
    }
}
