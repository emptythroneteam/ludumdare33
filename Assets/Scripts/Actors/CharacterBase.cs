﻿using UnityEngine;

public class CharacterBase : MonoBehaviour
{
    public float Speed = 1f;

    private Vector3 _to, _from;
    private bool _moving;
    private float _percent;
    private float _distance;

    private float _prevX;

    private float _left;
    private float _right;

    protected bool leftFacing;

    public virtual void Start()
    {
        _moving = false;
        _prevX = this.transform.position.x;
        _left = leftFacing ? 1f : -1f;
        _right = leftFacing ? -1f : 1f;
    }

    public virtual void Update()
    {
        var scale = 1f - (this.transform.position.y + 4.5f) * 1.25f / 8.5f;
        if (_moving)
        {
            _percent += Time.deltaTime * Speed * scale / _distance;
            this.transform.position = Vector3.Lerp(_from, _to, _percent);
            _moving = _percent <= 1f;

            if (!_moving)
            {
                AtDestination();
            }
        }

        var d = _prevX - this.transform.position.x;
        _prevX = this.transform.position.x;

        float facing;
        if(Mathf.Abs(d) > 10f * Mathf.Epsilon)
        {
            facing = d < 0 ? _right : _left;
        }
        else
        {
            facing = transform.localScale.x > 0 ? 1f : -1f;
        }

        this.transform.localScale = new Vector3(scale * facing, scale, 1f);
        this.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y / 10f);
    }

    public virtual void AtDestination() {}

    public virtual void GoTo(Vector3 target)
    {
        _moving = true;
        _from = this.transform.position;
        _to = target;
        _percent = 0f;
        _distance = Vector3.Distance(_from, _to);
    }

    public void Freeze()
    {
        _moving = false;
    }
}

