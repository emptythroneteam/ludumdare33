﻿using UnityEngine;

public class FamilyMember : DialogProvider
{
    public virtual void Start()
    {
        var scale = 1f - (this.transform.position.y + 4.5f) * 1.25f / 8.5f;
        this.transform.localScale = new Vector3(scale, scale, 1f);
        this.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y / 10f);
    }
}
