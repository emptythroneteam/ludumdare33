﻿using UnityEngine;

public class Popup : MonoBehaviour
{
    public float DialogFadeTime = 0.25f;

    private CanvasGroup _canvasGroup;

    private bool _visible;
    protected bool Visible
    {
        get
        {
            return _visible;
        }
        set
        {
            _visible = value;
            _canvasGroup.interactable = value;
            _canvasGroup.blocksRaycasts = value;
            if(!value)
            {
                Cleanup();
            }
        }
    }

    public virtual void Cleanup() {}
    public virtual void Hidden() {}

    public virtual void Start()
    {
        _canvasGroup = this.GetComponent<CanvasGroup>();
        _canvasGroup.alpha = 0;
        Visible = false;
    }

    // Update is called once per frame
    public virtual void Update()
    { 
        if (Visible)
        {
            _canvasGroup.alpha = Mathf.Clamp(_canvasGroup.alpha + Time.deltaTime / DialogFadeTime, 0f, 1f);
        }
        else
        {
            _canvasGroup.alpha = Mathf.Clamp(_canvasGroup.alpha - Time.deltaTime / DialogFadeTime, 0f, 1f);
            if(Mathf.Approximately(_canvasGroup.alpha, 0f))
            {
                Hidden();
            }
        }
    }
}