﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class UIController : MonoBehaviour
{
    public Sprite Market;
    public Sprite Home;

    public GameObject Bell;
    public GameObject Shears;
    public GameObject Food;
    public GameObject TeenToy;
    public GameObject GirlToy;
    public GameObject BoyToy;
    public GameObject Fence;
    public GameObject Repair;
    public GameObject Sheep;
    public GameObject Ram;

    private bool _market;
    private CanvasGroup _marketScreen;
    private Image _button;
    private Transform _itemPanel;
    private GameManager _gameManager;
    private Fence _fence;

    public void Start()
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _fence = GameObject.Find("Fence").GetComponent<Fence>();
        var market = this.transform.Find("Market");
        _marketScreen = market.GetComponent<CanvasGroup>();
        _button = this.transform.Find("HUD").transform.Find("Travel").GetComponent<Image>();
        _itemPanel = market.transform.Find("ItemPanel");
        _market = false;
        _button.sprite = Market;
        _marketScreen.alpha = 0f;
        _marketScreen.interactable = false;
        _marketScreen.blocksRaycasts = false;
    }

    public void ToggleMarket()
    {
        _market = !_market;
        if(_market)
        {
            Populate();
            _button.sprite = Home;
            _marketScreen.alpha = 1f;
            _marketScreen.interactable = true;
            _marketScreen.blocksRaycasts = true;

        }
        else
        {
            Clear();
            _button.sprite = Market;
            _marketScreen.alpha = 0f;
            _marketScreen.interactable = false;
            _marketScreen.blocksRaycasts = false;
        }
    }

    private void Clear()
    {
        for(int i = 0; i < _itemPanel.childCount; i++)
        {
            Destroy(_itemPanel.GetChild(i).gameObject);
        }
    }

    private void Populate()
    {
        int x = 0;
        int y = 0;
        foreach(var item in GetItems())
        {
            item.transform.SetParent(_itemPanel);
            item.transform.localPosition = new Vector3(x * 125f - 250f, -y * 118f + 236f, 0f);
            item.transform.localScale = Vector3.one;
            x++;
            if(x == 4)
            {
                x = 0;
                y++;
            }
        }
    }

    private List<GameObject> GetItems()
    {
        if (_gameManager.Day == 1)
        {
            return new List<GameObject> { Instantiate(Bell) };
        }

        var ret = new List<GameObject>();

        var food = Instantiate(Food);
        food.GetComponent<Food>().Tier = _gameManager.FoodTier;
        ret.Add(food);

        if (_gameManager.BonusScore == 0)
        {
            ret.Add(Instantiate(Shears));
        }

        var tiers = new List<int> { 0 };
        if(_gameManager.Day > 1)
        {
            tiers.Add(1);
            ret.Add(Instantiate(Repair));
        }
        if(_gameManager.Day > 2)
        {
            tiers.Add(2);
        }
        if(_gameManager.Day > 3)
        {
            tiers.Add(3);
        }
        tiers.Remove(_fence.Level);

        ret.AddRange(tiers.Select(t =>
        {
            var f = Instantiate(Fence);
            f.GetComponent<FenceItem>().Tier = t;
            return f;
        }));

        if(_gameManager.GetCondition("emmanotoy"))
        {
            ret.Add(Instantiate(TeenToy));
        }
        if (_gameManager.GetCondition("ericnotoy"))
        {
            ret.Add(Instantiate(BoyToy));
        }
        if (_gameManager.GetCondition("lisanotoy"))
        {
            ret.Add(Instantiate(GirlToy));
        }
        if(_gameManager.AnimalCount < 8)
        {
            ret.Add(Instantiate(Sheep));
            ret.Add(Instantiate(Ram));
        }

        return ret;
    }
}
