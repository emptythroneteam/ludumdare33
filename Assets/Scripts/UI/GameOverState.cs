﻿public enum GameOverState
{
    Hidden,

    GameOver,

    GoodEnd,

    BadEnd
}