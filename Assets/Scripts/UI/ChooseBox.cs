﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;

public class ChooseBox : Popup
{
    public Button ButtonPrefab;

    public void ShowOptions(List<Choice> choices)
    {
        var i = 0;
        foreach(var button in this.GetComponentsInChildren<Button>())
        {
            button.onClick.RemoveAllListeners();

            if(i < choices.Count)
            {
                button.onClick.AddListener(choices[i].Callback);
                button.onClick.AddListener(() => Visible = false);
                button.GetComponentInChildren<Text>().text = choices[i].Text;
            }
            else
            {
                button.GetComponentInChildren<Text>().text = string.Empty;
            }
            i++;
        }
        Visible = true;
    }
}

public class Choice
{
    public string Text;

    public UnityAction Callback;
}