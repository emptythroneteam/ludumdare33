﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Color _color, _highlight;
    private Text _text;

    public void Start()
    {
        _text = this.GetComponentInChildren<Text>();
        _color = _text.color;
        _highlight = Color.white;
    } 
   
    public void OnPointerEnter(PointerEventData eventData)
    {
        _text.color = _highlight;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _text.color = _color;
    }
}
