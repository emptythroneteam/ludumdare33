﻿public class SheepItem : ItemBase
{
    public override int Price
    {
        get
        {
            return 5 + GameManager.SheepPurchased * 5;
        }
    }

    public override string Description
    {
        get
        {
            return "Need some new sheep for your flock, eh? Remember, these are going to get expensive if you keep getting them. You should probably focus on keeping yours safe.";
        }
    }

    protected override bool Single { get { return false; } }

    public override void Action()
    {
        GameManager.SheepPurchased++;
        GameManager.SpawnAnimal(GameManager.Sheep);
    }
}
