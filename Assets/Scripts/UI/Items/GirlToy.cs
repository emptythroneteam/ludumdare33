﻿public class GirlToy : ItemBase
{
    public override int Price
    {
        get
        {
            return 10;
        }
    }


    public override string Description
    {
        get
        {
            return "Your daughter’s going to be a fiery young lady. I think she’ll enjoy this.";
        }
    }

    public override void Action()
    {
        GameManager.SetCondition("lisatoy", true);
        GameManager.SetCondition("lisanotoy", false);
    }
}
