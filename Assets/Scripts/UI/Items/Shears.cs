﻿public class Shears : ItemBase
{
    public override int Price
    {
        get
        {
            return 50;
        }
    }

    public override string Description
    {
        get
        {
            return "I honestly have no idea where we got these. They’re really really sharp though, it’s kind of scary. Maybe you can use them to get a closer shave on your sheep?";
        }
    }

    public override void Action()
    {
        GameManager.BonusScore = 10;
    }
}
