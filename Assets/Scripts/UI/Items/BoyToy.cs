﻿public class BoyToy : ItemBase
{
    public override int Price
    {
        get
        {
            return 10;
        }
    }

    public override string Description
    {
        get
        {
            return "I think I heard that Eric was looking for something to play with? Every boy loves trains, right?";
        }
    }

    public override void Action()
    {
        GameManager.SetCondition("erictoy", true);
        GameManager.SetCondition("ericnotoy", false);
    }
}
