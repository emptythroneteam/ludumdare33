﻿using UnityEngine;

public class Repair : ItemBase
{
    private Fence Fence;

    public override int Price
    {
        get
        {
            return Fence.Cost - 5 + 3 * Fence.RepairCount;
        }
    }

    public override string Description
    {
        get
        {
            return "Use this to repair a broken fence. It’s going to get harder to keep patching one up though.";
        }
    }

    public override void Action()
    {
        Fence.Repair();
    }

    public override void Start()
    {
        base.Start();
        Fence = GameObject.Find("Fence").GetComponent<Fence>();
    }
}
