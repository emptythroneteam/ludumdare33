﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class ItemBase : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    protected GameManager GameManager { get; set;}

    protected Text PriceText { get; set; }
    
    protected Text MayaText { get; set; }

    protected Image Icon { get; set; }

    protected virtual bool Single { get { return true; } }

    public abstract int Price { get; }

    public abstract string Description { get; }

    public abstract void Action();

    public void Click()
    {
        if(GameManager.Money >= Price)
        {
            GameManager.RemoveMoney(Price);
            Action();
            if(Single)
            {
                MayaText.text = "Welcome!";
                Destroy(this.gameObject);
            }
            
        }
    }

    public virtual void Start()
    {
        MayaText = GameObject.Find("MayaText").GetComponent<Text>();
        GameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        PriceText = this.GetComponentInChildren<Text>();
        Icon = this.GetComponent<Image>();
    }

    public virtual void Update()
    {
        PriceText.text = Price.ToString();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        MayaText.text = Description;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        MayaText.text = "Welcome!";
    }
}
