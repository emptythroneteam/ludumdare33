﻿using UnityEngine;
using System.Collections.Generic;

public class FenceItem : ItemBase
{
    private readonly List<string> Descriptions = new List<string>
    {
        "Just a simple wooden fence, nothing to write home about.",
        "Now we’re getting somewhere! I sharpened the sticks myself! This’ll keep the wolves at bay.",
        "Things must be getting bad up there, this wall will keep out damn near anything.",
        "Best of both worlds, a stone wall with sharpened spikes. What more could you ask for?",
    };

    public List<Sprite> Icons;

    public int Tier;

    private Fence Fence;

    public override int Price
    {
        get
        {
            if(Tier == 0)
            {
                return 5;
            }

            return 10 + Tier * 5;
        }
    }

    public override string Description
    {
        get
        {
            return Descriptions[Tier];
        }
    }

    public override void Action()
    {
        Fence.Broken = false;
        Fence.RepairCount = 0;
        Fence.Level = Tier;
    }

    public override void Update()
    {
        base.Update();
        Icon.sprite = Icons[Tier];
        Fence = GameObject.Find("Fence").GetComponent<Fence>();
    }
}
