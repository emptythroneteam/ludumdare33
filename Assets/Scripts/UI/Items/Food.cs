﻿using UnityEngine;
using System.Collections.Generic;

public class Food : ItemBase
{
    private readonly List<string> Descriptions = new List<string>
    {
        "Baked it this morning!",
        "Perfect for a pie…",
        "Well, I like beets. Just saying.",
        "Well it’s not laying eggs anymore. Might as well take it home with you.",
        "I’m sure everyone’s going to love this. It comes from the steer farm just down the way… come to think of it, I haven’t heard from them in a little while. Spooky.",
        "Salted, seasoned, and prepared to perfection. I’m kind of really proud of this one.",
    };

    public int Tier;

    public List<Sprite> Icons;

    public override int Price
    {
        get
        {
            return 5 + 5 * Tier;
        }
    }

    public override string Description
    {
        get
        {
            return Descriptions[Tier];
        }
    }

    public override void Action()
    {
        Icon.sprite = Icons[Tier - 1];
        GameManager.FoodTier++;
        GameManager.SetCondition("nofood", false);
    }

    public override void Update()
    {
        base.Update();
        Icon.sprite = Icons[Tier];
    }
}
