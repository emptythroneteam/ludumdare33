﻿using System;

public class BellItem : ItemBase
{
    public override int Price
    {
        get
        {
            return 25;
        }
    }

    public override string Description
    {
        get
        {
            return "I uh… heard that your bell broke a while back.We just got this one in. You’ll need it to call everyone in for the day.";
        }
    }

    public override void Action()
    {
        GameManager.ShowBell();
    }
}
