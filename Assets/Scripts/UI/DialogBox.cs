﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class DialogBox : Popup
{
    private Text Text;

    private Text Name;

    private Image Portrait;

    private int _index;

    private List<GameManager.ConversationEntry> _entries;

    private Action _callback;

    private GameManager _gameManger;

    public DialogMode Mode;

    public override void Start()
    {
        base.Start();

        Mode = DialogMode.Random;
        _gameManger = GameObject.Find("GameManager").GetComponent<GameManager>();
        Text = this.transform.Find("Text").GetComponent<Text>();
        Name = this.transform.Find("Name").GetComponent<Text>();
        Portrait = this.transform.Find("DialogPortrait").GetComponent<Image>();
    }

    public void ShowDialog(string name, string dialog, Sprite portrait)
    {
        Text.text = dialog;
        Name.text = name + ":";
        Portrait.sprite = portrait;
        Visible = true;
    }

    public void ShowCoversation(List<GameManager.ConversationEntry> conversation, Action callback)
    {
        Mode = DialogMode.Conversation;
        _entries = conversation;
        _index = 0;
        _callback = callback;
        _gameManger.ShowMonster = _entries[0].ShowMonster;
        ShowDialog(_entries[0].Name, _entries[0].Line, _entries[0].Portrait);
    }

    public override void Hidden()
    {
        if(Mode == DialogMode.Random)
        {
            return;
        }

        if (_index < _entries.Count)
        {
            GameOver.State = _entries[_index].GameOverState;
            _gameManger.ShowMonster = _entries[_index].ShowMonster;
            ShowDialog(_entries[_index].Name, _entries[_index].Line, _entries[_index].Portrait);
            Visible = true;
        }
        else
        {
            _callback();
            Mode = DialogMode.Random;
            _index = 0;
            _callback = null;
            _entries = null;
            Visible = false;
        }
    }

    public override void Update()
    {
        base.Update();
        
        if(Input.GetMouseButtonDown(0))
        {
            if(Mode == DialogMode.Conversation)
            {
                _index++;
            }

            Visible = false;
        }
        
    }
}
