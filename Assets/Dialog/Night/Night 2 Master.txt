[]
SOUNDCUE#StartNightSound
SOUNDCUE#StartDinnerSound
Simon#We got a lot of work done today. I think we deserve something special!
Mother#Mm…
Emma#NoFood#Like we can afford it.
Simon#NoFood#We have plenty of cabbage-
Emma#NoFood#Oh boy, stewed cabbage… I'll start cooking.
Emma#Food#Yeah, bread, real special. 
Eric#Food#I like bread!
Simon#Food#That's right, dear. You should be grateful for what you have.
Emma#Food#Better than stewed cabbage, I guess.
Lisa#Papa, I heard something while I was out today.
Eric#Me too!
Simon#Oh?
Lisa#No, you didn't, you liar!
Eric#Yes I did! I heard something in the woods, something moving around there. 
Lisa#No! You've been yelling all day and making a… ruckus!
Eric#Mama!
Simon#Calm down. I believe you, Eric. Those woods are old and deep.
Simon#…and we need to build sturdier fencing…
Emma#Can we afford that?
Simon#We'll make do. Anyway, children, stay away from the woods. Keep inside at night.
Eric#I'm not afraid!
Lisa#A wolf doesn't care if you're afraid or not.
Emma#Would you two stop talking about this? It's creeping me out.
Simon#Sweet pea, your cabbage?
Emma#Wha- Oh, damn it!
SOUNDCUE#StopAllSound
